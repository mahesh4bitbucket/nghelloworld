import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  students = [
    {rollno : 1, name : "Neha", address : "Shrirampur"},
    {rollno : 2, name : "Ashwini", address : "A'bad"},
    {rollno : 3, name : "Priyanka", address : "Nagar"},
    {rollno : 4, name : "Shital", address : "Ahmednagar"},
    {rollno : 5, name : "Poonam", address : "Barshi"}
  ];

  constructor() { }
  getData () {
    return this.students;

  }
}
