import { Component, OnInit } from '@angular/core';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-studentdetail',
  templateUrl: './studentdetail.component.html',
  styleUrls: ['./studentdetail.component.css']
})
export class StudentdetailComponent implements OnInit {

  students = [];  
  constructor(private stuService : StudentService) { }


  ngOnInit() {
    this.students = this.stuService.getData();
  }

}
