import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  c = 0;
  // ekvariable = [];
  constructor() { }

  ngOnInit() {
    console.log(this.getData().name);
  }

  getData(){
    //return this.c = 10+10;
    //return "I'm from function..";
    //return a+b;
    // return [10,20,30,40,50];
    return {name : "ABC", city: "Shrirampur"};
  }

  // ekvariable = this.getData();
  // ekvariable = [10,20,30,40,50];
  
  //string pipe
  name = "poonam doke";
  company = "technogrowth";

  price = 1;

  date = new Date();

}
